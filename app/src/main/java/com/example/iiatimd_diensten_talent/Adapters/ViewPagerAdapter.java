package com.example.iiatimd_diensten_talent.Adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.iiatimd_diensten_talent.R;

public class ViewPagerAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater inflater;

    public ViewPagerAdapter(Context context) {
        this.context = context;
    }

    private int images[] = {
            R.drawable.p1,
            R.drawable.p2,
            R.drawable.p3,
            R.drawable.p4,
    };

    private String titles[] = {
            "Werk voor elkaar en ruil diensten uit",
            "Plaats een advertentie",
            "Ontvang reacties",
            "Neem contact op"
    };

    private String descs[] = {
            "Door diensten te ruilen met elkaar kun je vaardigheden delen. Deel je talenten met de community om zo een gewenste dienst terug te ontvangen.",
            "Plaats een advertentie met een verduidelijkende afbeelding en een beschrijving. Beschrijf je eigen kunnen en wat je terug wilt krijgen. Sta je open voor alle aanbiedingen? Geef het aan in je bericht en ontvang reacties.",
            "Nadat je een advertentie geplaatst hebt is het tijd om reacties te ontvangen van alle community leden. De mensen die reageren zijn vaak geïnteresseerd in wat jij kan leveren of in wat jij wilt ontvangen.",
            "Van alle ontvangen reacties kun je bepalen met wie je in contact wilt komen. Neem contact op met elkaar en zo is de ruildienst een feit geworden. Veel plezier!"
    };

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (LinearLayout)object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.view_pager, container, false);

        ImageView imageView = v.findViewById(R.id.imgViewPager);
        TextView txtTitle = v.findViewById(R.id.txtTitleViewPager);
        TextView txtDesc = v.findViewById(R.id.txtDescViewPager);

        imageView.setImageResource(images[position]);
        txtTitle.setText(titles[position]);
        txtDesc.setText(descs[position]);

        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout)object);
    }
}
