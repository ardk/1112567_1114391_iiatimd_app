# IIATIMD
Deze applicatie is gerealiseerd tijdens de module IIATIMD.

## Diensten Talent
Diensten Talent is een android applicatie waarbij freelancers hun diensten kunnen aanbieden in ruil voor andere diensten. Om dit concept te verduidelijken volgt hier een praktisch voorbeeld: Een webdesigner biedt een responsive website aan in ruil voor een nieuwe voortuin. Of een schilder die de kozijnen komt schilderen in ruil voor een nieuwe grasmat in zijn achtertuin.

Deze applicatie werkt in combinatie met de API die gemaakt is in Laravel. Alle authenticatie met betrekking tot de accounts gaat via JWT en alle data wordt opgeslagen in onze database.

## Informatie
De git flow die wij hanteren tijdens dit project is als volgt. Per User Story of nieuwe feature wordt een nieuwe branch gemaakt die afstamt van de Development branch. Vervolgens wordt er een pull request aangemaakt die door de teamgenoot gecheckt wordt middels een code review. Na een approve wordt deze gemerged op de development branch. Voor de oplevering zal de code op de master branch gemerged worden.

## Installatie

1. Clone de repository
2. Open het project in Android Studio
3. Make project / gradle build
4. Start applicatie via emulator of telefoon

```bash
git clone https://<NAAM>@bitbucket.org/ardk/1112567_1114391_iiatimd_app.git
```
